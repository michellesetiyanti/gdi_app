import 'package:flutter/material.dart';
import 'package:gdi_app/pages/sign_in.dart';

import 'package:get/get.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Grha Digital Indonesia',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'DMSans',
        // scaffoldBackgroundColor: Color.fromARGB(255, 255, 250, 250),
      ),
      home: const SignIn(),
    );
  }
}
