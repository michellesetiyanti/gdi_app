import 'package:flutter/material.dart';
import 'package:gdi_app/pages/admin/pilihkaryawan_admin.dart';
import 'package:gdi_app/widgets/date_picker_field.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../../widgets/default/text_widget.dart';
import '../../widgets/laporan_field.dart';
import '../../widgets/submit_button.dart';
import '../../widgets/tracking_profile_photo.dart';

class AddProjectAdmin extends StatelessWidget {
  const AddProjectAdmin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 75,
        title: const TextWidget(
          text: "Tambah Project",
          size: 20,
          fw: FontWeight.w600,
          color: Colors.black,
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.black,
          ),
        ),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: LaporanField(
                  title: "Judul Project", hint: "Masukkan Judul Project"),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: LaporanField(
                  title: "Tahap Pengerjaan",
                  hint: "Masukkan Tahap Pengerjaan",
                  maxliness: 2),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextWidget(
                text: "Tanggal Deadline",
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  Flexible(flex: 1, child: DatePickerField()),
                  SizedBox(width: 10),
                  Flexible(flex: 0, child: TextWidget(text: "sd")),
                  SizedBox(width: 10),
                  Flexible(flex: 1, child: DatePickerField()),
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: TextWidget(
                text: "Pilih Karyawan",
                fw: FontWeight.w500,
                color: Color.fromRGBO(24, 29, 39, 1),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Row(
                children: [
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 6),
                    child: TrackingProfilePhoto(),
                  ),
                  const SizedBox(width: 2),
                  const TrackingProfilePhoto(),
                  const SizedBox(width: 2),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      color: const Color.fromRGBO(0, 128, 255, 1),
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: IconButton(
                      onPressed: () {
                        Get.to(() => const PilihKaryawan());
                      },
                      icon: const Icon(Icons.add, color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: LaporanField(title: "Status", hint: "Masukkan Status"),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const TextWidget(
                    text: "Progress",
                    fw: FontWeight.w500,
                    color: Color.fromRGBO(24, 29, 39, 1),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                          onPressed: () {}, icon: const Icon(Icons.remove)),
                      IconButton(onPressed: () {}, icon: const Icon(Icons.add))
                    ],
                  ),
                  //progress indicator
                  LinearPercentIndicator(
                    animation: true,
                    animationDuration: 1000,
                    lineHeight: 10,
                    percent: 0.7,
                    trailing: const TextWidget(
                      text: "70%",
                      fw: FontWeight.w500,
                    ),
                    backgroundColor: Colors.grey,
                    progressColor: const Color.fromRGBO(0, 128, 255, 1),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 58, 20, 30),
              child: SubmitButton(
                text: "Simpan",
                width: double.infinity,
                height: 55,
                pressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}
