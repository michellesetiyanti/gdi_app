import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gdi_app/pages/admin/tracking_page_adminn.dart';
import 'package:get/get.dart';

import '../../widgets/default/text_widget.dart';
import '../../widgets/menu_button.dart';

class HomepageAdmin extends StatelessWidget {
  const HomepageAdmin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: double.infinity,
            padding: const EdgeInsets.fromLTRB(20, 88, 20, 28),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                TextWidget(
                  text: "Hello Admin,",
                  fw: FontWeight.w700,
                  size: 21,
                ),
                SizedBox(height: 5),
                TextWidget(
                  text: "Selamat datang di Grha Digital Indonesia",
                  size: 13,
                  color: Color.fromRGBO(85, 85, 85, 1),
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 35, vertical: 13),
            child: Row(
              children: [
                Expanded(
                  child: MenuButton(
                    icon: FontAwesomeIcons.clipboardList,
                    title: "Tracking",
                    pressed: () {
                      Get.to(() => const TrackingPageAdmin());
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
