import 'package:flutter/material.dart';
import 'package:gdi_app/widgets/tracking_search_bar.dart';
import 'package:get/get.dart';

import '../../widgets/default/text_widget.dart';
import '../../widgets/karyawan_card.dart';

class PilihKaryawan extends StatelessWidget {
  const PilihKaryawan({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 75,
        title: const TextWidget(
          text: "Pilih Karyawan",
          size: 20,
          fw: FontWeight.w600,
          color: Colors.black,
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.black,
          ),
        ),
      ),
      body: Column(
        children: const [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            child: TrackingSearchBar(hintText: "Search"),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: KaryawanCard(),
          )
        ],
      ),
    );
  }
}
