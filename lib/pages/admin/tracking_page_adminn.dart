import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../widgets/default/text_widget.dart';
import '../../widgets/tracking_admin_card.dart';
import '../../widgets/tracking_search_bar.dart';

import 'add_project_admin.dart';

class TrackingPageAdmin extends StatelessWidget {
  const TrackingPageAdmin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // iconTheme: const IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 75,
        title: const TextWidget(
          text: "Tracking Project",
          size: 20,
          fw: FontWeight.w600,
          color: Colors.black,
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.black,
          ),
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(right: 20, bottom: 50),
        child: FloatingActionButton(
          elevation: 2,
          backgroundColor: const Color.fromRGBO(0, 128, 255, 1),
          onPressed: () {
            Get.to(() => const AddProjectAdmin());
          },
          child: const Icon(
            Icons.add,
          ),
        ),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: const [
            //search bar
            Padding(
              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 20),
              child: TrackingSearchBar(hintText: "Masukkan Nomor HP",),
            ),

            //konten
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: TrackingAdminCard(),
            ),
          ],
        ),
      ),
    );
  }
}

