import 'package:flutter/material.dart';
import 'package:gdi_app/widgets/laporan_field.dart';
import 'package:gdi_app/widgets/submit_button.dart';
import 'package:get/get.dart';

import '../../widgets/default/text_widget.dart';

class AddPesan extends StatelessWidget {
  const AddPesan({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 75,
        title: const TextWidget(
          text: "Contact Us",
          size: 20,
          fw: FontWeight.w600,
          color: Colors.black,
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.black,
          ),
        ),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: LaporanField(
                  title: "Nama Panjang", hint: "Masukkan Nama Panjang"),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: LaporanField(title: "Email", hint: "Masukkan Email"),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: LaporanField(title: "Pesan", hint: "Masukkan Pesan"),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: LaporanField(
                title: "Detail Pesan",
                hint: "Masukkan Detail Pesan",
                maxliness: 5,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
              child: SubmitButton(
                  text: "Kirim",
                  height: 55,
                  width: double.infinity,
                  pressed: () {}),
            )
          ],
        ),
      ),
    );
  }
}
