import 'package:flutter/material.dart';

import '../../widgets/default/text_widget.dart';

import 'package:get/get.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';

import '/controller/chat_room_ctrl.dart';
// class ContactUsPage extends StatelessWidget {
//   const ContactUsPage({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.transparent,
//         elevation: 0,
//         toolbarHeight: 75,
//         title: const TextWidget(
//           text: "Pesan",
//           size: 20,
//           fw: FontWeight.w600,
//           color: Colors.black,
//         ),
//         centerTitle: true,
//         leading: IconButton(
//           onPressed: () {
//             Get.back();
//           },
//           icon: const Icon(
//             Icons.arrow_back_ios_rounded,
//             color: Colors.black,
//           ),
//         ),
//       ),
//       floatingActionButton: Padding(
//         padding: const EdgeInsets.only(right: 20, bottom: 50),
//         child: FloatingActionButton(
//           elevation: 2,
//           backgroundColor: const Color.fromRGBO(0, 128, 255, 1),
//           onPressed: () {
//             Get.to(() => const AddPesan());
//           },
//           child: const Icon(
//             Icons.add,
//           ),
//         ),
//       ),
//     );
//   }
// }

class ContactUsPage extends StatelessWidget {
  ContactUsPage({Key? key}) : super(key: key);

  final controllerChat = Get.put(ChatRoomController());

  final Uri _url =
      Uri.parse('https://api.whatsapp.com/send?phone=6281803887878');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: const IconThemeData(color: Colors.black),
        // titleTextStyle: const TextStyle(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
        leadingWidth: 100,
        leading: InkWell(
          onTap: () => Get.back(),
          borderRadius: BorderRadius.circular(100),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const SizedBox(width: 5),
              const Icon(Icons.arrow_back_ios_new),
              const SizedBox(width: 5),
              ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: Image.asset('assets/images/anya.jpg',
                    height: 45, width: 45),
              ),
            ],
          ),
        ),
        actions: [
          SizedBox(
            height: 40,
            width: 60,
            child: InkWell(
              borderRadius: BorderRadius.circular(100),
              onTap: () async {
                if (!await launchUrl(_url,
                    mode: LaunchMode.externalApplication)) {
                  throw 'Could not launch $_url';
                }
              },
              child: const Icon(FontAwesomeIcons.whatsapp, color: Colors.green),
            ),
          )
        ],
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            TextWidget(
              text: "Grha Digital Indonesia",
              size: 14,
              color: Color.fromRGBO(24, 29, 39, 1),
              fw: FontWeight.w600,
            ),
            TextWidget(
              text: "Admin",
              size: 12,
              color: Color.fromRGBO(126, 126, 126, 1),
            )
          ],
        ),
      ),
      body: WillPopScope(
        onWillPop: () {
          if (controllerChat.isShowEmoji.isTrue) {
            controllerChat.isShowEmoji.value = false;
          } else {
            Navigator.pop(context);
          }
          return Future.value(false);
        },
        child: Column(
          children: [
            Expanded(
              child: ListView(
                reverse: true,
                physics: const BouncingScrollPhysics(),
                children: const [
                  ChatItem(isSender: true),
                  ChatItem(isSender: false),
                  ChatItem(isSender: true),
                  ChatItem(isSender: false),
                  ChatItem(isSender: true),
                  ChatItem(isSender: false),
                  ChatItem(isSender: true),
                  ChatItem(isSender: false),
                  ChatItem(isSender: true),
                  ChatItem(isSender: false),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 20, right: 15),
              height: 100,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(112, 144, 176, 0.2),
                      blurRadius: 20),
                ],
              ),
              child: Container(
                margin: EdgeInsets.only(
                  bottom: controllerChat.isShowEmoji.isTrue
                      ? 5
                      : context.mediaQueryPadding.bottom,
                ),
                // padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                width: Get.width,
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                        controller: controllerChat.chatC,
                        focusNode: controllerChat.focusNode,
                        decoration: InputDecoration(
                          prefixIcon: IconButton(
                            onPressed: () {
                              controllerChat.focusNode.unfocus();
                              controllerChat.isShowEmoji.toggle();
                            },
                            icon: const Icon(Icons.emoji_emotions_rounded),
                          ),
                          hintText: "Type here...",
                          hintStyle: const TextStyle(
                              color: Color.fromRGBO(126, 126, 126, 0.7)),
                          filled: true,
                          fillColor: const Color.fromRGBO(242, 242, 242, 1),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            borderSide: const BorderSide(
                              width: 0,
                              style: BorderStyle.none,
                            ),
                          ),
                        ),
                      ),
                    ),
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.send),
                      color: const Color.fromRGBO(0, 128, 255, 1),
                    )
                  ],
                ),
              ),
            ),
            Obx(
              () => (controllerChat.isShowEmoji.isTrue)
                  ? SizedBox(
                      height: 250,
                      child: EmojiPicker(
                        onEmojiSelected: (category, emoji) {
                          controllerChat.addEmojiToChat(emoji);
                        },
                        onBackspacePressed: () {
                          controllerChat.deleteEmoji();
                        },
                        config: const Config(),
                      ),
                    )
                  : const SizedBox(),
            ),
          ],
        ),
      ),
    );
  }
}

class ChatItem extends StatelessWidget {
  const ChatItem({Key? key, required this.isSender}) : super(key: key);

  final bool isSender;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(20, 20, 20, 15),
      // color: Colors.green,
      child: Column(
        crossAxisAlignment:
            isSender ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
              color: isSender
                  ? const Color.fromRGBO(0, 128, 255, 1)
                  : const Color.fromRGBO(242, 242, 242, 1),
              borderRadius: isSender
                  ? const BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                      bottomLeft: Radius.circular(15),
                    )
                  : const BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15),
                      bottomRight: Radius.circular(15),
                    ),
            ),
            padding: const EdgeInsets.all(10),
            child: Text(
              "Halooo, selamat datang",
              style: TextStyle(color: isSender ? Colors.white : Colors.black),
            ),
          ),
          const SizedBox(height: 5),
          const Text("04:25 AM"),
        ],
      ),
      alignment: isSender ? Alignment.centerRight : Alignment.centerLeft,
    );
  }
}
