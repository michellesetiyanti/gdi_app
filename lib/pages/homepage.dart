import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gdi_app/pages/tracking/tracking_page.dart';
import 'package:get/get.dart';

import 'contact us/contactus_page.dart';
import 'laporan/laporan_page.dart';
import 'portfolio/portfolio_detail_page.dart';
import 'portfolio/portfolio_page.dart';

import '../widgets/default/text_widget.dart';
import '../widgets/jasa_kami.dart';
import '../widgets/launch_gdi.dart';
import '../widgets/menu_button.dart';
import '../widgets/portfolio_stack.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  final Uri _url = Uri.parse('https://www.grhadigital.id/');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              padding: const EdgeInsets.fromLTRB(20, 88, 20, 28),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  TextWidget(
                    text: "Hello Michelle,",
                    fw: FontWeight.w700,
                    size: 21,
                  ),
                  SizedBox(height: 5),
                  TextWidget(
                    text: "Selamat datang di Grha Digital Indonesia",
                    size: 13,
                    color: Color.fromRGBO(85, 85, 85, 1),
                  ),
                ],
              ),
            ),

            //menu button
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: GridView.count(
                mainAxisSpacing: 15,
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                crossAxisCount: 4,
                children: [
                  MenuButton(
                    icon: FontAwesomeIcons.clipboardList,
                    title: "Tracking",
                    pressed: () {
                      Get.to(() => const TrackingPage());
                    },
                  ),
                  MenuButton(
                    icon: FontAwesomeIcons.fileSignature,
                    title: "Portfolio",
                    pressed: () {
                      Get.to(() => const PortfolioPage());
                    },
                  ),
                  MenuButton(
                    icon: Icons.phone,
                    title: "Contact Us",
                    pressed: () {
                      Get.to(() => ContactUsPage());
                    },
                  ),
                  MenuButton(
                    icon: Icons.headset_mic,
                    title: "Lapor",
                    pressed: () {
                      Get.to(() => const LaporanPage());
                    },
                  ),
                ],
              ),
            ),

            LaunchGDI(url: _url),

            //portfolio
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child:
                  TextWidget(text: "Portfolio", size: 21, fw: FontWeight.w700),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              physics: const BouncingScrollPhysics(),
              child: Row(
                children: [
                  PortfolioStack(
                    projek: "Gemilang Total Optima",
                    dev: "Web Development",
                    heightgambar: 165,
                    widthgambar: 295,
                    heightbutton: 50,
                    widthbutton: 254,
                    pressed: () {
                      Get.to(() => const PortfolioDetail());
                    },
                  ),
                  PortfolioStack(
                    projek: "Sayur Mama",
                    dev: "Mobile Development",
                    heightgambar: 165,
                    widthgambar: 295,
                    heightbutton: 50,
                    widthbutton: 254,
                    pressed: () {},
                  ),
                  PortfolioStack(
                    projek: "BPOM Pontianak",
                    dev: "Mobile & Web Development",
                    heightgambar: 165,
                    widthgambar: 295,
                    heightbutton: 50,
                    widthbutton: 254,
                    pressed: () {},
                  ),
                ],
              ),
            ),

            //Jasa
            const Padding(
              padding: EdgeInsets.fromLTRB(20, 20, 20, 15),
              child:
                  TextWidget(text: "Jasa Kami", size: 21, fw: FontWeight.w700),
            ),
            const JasaKami(jasa: "Web Development"),
            const JasaKami(jasa: "Mobile Development"),

            const SizedBox(height: 50)
          ],
        ),
      ),
    );
  }
}
