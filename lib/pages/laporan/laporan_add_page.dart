import 'package:flutter/material.dart';
import 'package:gdi_app/widgets/submit_button.dart';
import 'package:get/get.dart';
import 'package:file_picker/file_picker.dart';

import '../../widgets/default/text_widget.dart';
import '../../widgets/laporan_upload_file.dart';
import '../../widgets/laporan_field.dart';

class LaporanAdd extends StatelessWidget {
  const LaporanAdd({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 75,
        title: const TextWidget(
          text: "Laporan",
          size: 20,
          fw: FontWeight.w600,
          color: Colors.black,
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.black,
          ),
        ),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: LaporanField(
                  title: "Jenis Laporan", hint: "Masukkan Jenis Laporan"),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: UploadFile(
                pressed: () async {
                  String? selectedDirectory =
                      await FilePicker.platform.getDirectoryPath();

                  if (selectedDirectory == null) {
                    // User canceled the picker
                  }
                },
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: LaporanField(
                title: "Keterangan",
                hint: "Masukkan Keterangan",
                maxliness: 5,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: SubmitButton(
                text: "Simpan",
                width: double.infinity,
                height: 55,
                pressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}
