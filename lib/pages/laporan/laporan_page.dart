import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'laporan_add_page.dart';

import '../../widgets/default/container_widget.dart';
import '../../widgets/default/text_widget.dart';

class LaporanPage extends StatelessWidget {
  const LaporanPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 75,
        title: const TextWidget(
          text: "Laporan",
          size: 20,
          fw: FontWeight.w600,
          color: Colors.black,
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.black,
          ),
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(right: 20, bottom: 50),
        child: FloatingActionButton(
          elevation: 2,
          backgroundColor: const Color.fromRGBO(0, 128, 255, 1),
          onPressed: () {
            Get.to(() => const LaporanAdd());
          },
          child: const Icon(
            Icons.add,
          ),
        ),
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
            child: ContainerWidget(
              radius: BorderRadius.circular(20),
              paddding: const EdgeInsets.all(20),
              childd: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  TextWidget(
                    text: "#Ticket19052201",
                    size: 12,
                    color: Color.fromRGBO(142, 142, 142, 1),
                  ),
                  SizedBox(height: 8),
                  TextWidget(
                    text: "Tombol tidak bisa ditekan",
                    size: 16,
                    fw: FontWeight.w700,
                    color: Color.fromRGBO(24, 29, 39, 1),
                  ),
                  SizedBox(height: 6),
                  TextWidget(
                    text:
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla...",
                    fw: FontWeight.w500,
                    color: Color.fromRGBO(164, 164, 164, 1),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
