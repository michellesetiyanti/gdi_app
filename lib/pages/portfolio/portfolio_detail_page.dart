import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../widgets/default/text_widget.dart';

class PortfolioDetail extends StatelessWidget {
  const PortfolioDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // iconTheme: const IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 75,
        title: const TextWidget(
          text: "Portfolio",
          size: 20,
          fw: FontWeight.w600,
          color: Colors.black,
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.black,
          ),
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 20),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.asset(
                "assets/images/pf.jpg",
                width: double.infinity,
                height: 183,
                fit: BoxFit.cover,
              ),
            ),
          ),
          const TextWidget(
            text: "Gemilang Total Optima",
            size: 16,
            fw: FontWeight.w600,
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: SizedBox(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  TextWidget(
                    text: "Creator",
                    size: 12,
                    fw: FontWeight.w500,
                    color: Color.fromRGBO(160, 160, 160, 1),
                  ),
                  SizedBox(height: 6),
                  TextWidget(
                    text: "PT Grha Digital Indonesia",
                    fw: FontWeight.w500,
                    color: Color.fromRGBO(24, 29, 39, 1),
                  ),
                  SizedBox(height: 20),
                  TextWidget(
                    text: "Product",
                    size: 12,
                    fw: FontWeight.w500,
                    color: Color.fromRGBO(160, 160, 160, 1),
                  ),
                  SizedBox(height: 6),
                  TextWidget(
                    text: "Website Development",
                    fw: FontWeight.w500,
                    color: Color.fromRGBO(24, 29, 39, 1),
                  ),
                  SizedBox(height: 20),
                  TextWidget(
                    text: "Description",
                    size: 12,
                    fw: FontWeight.w500,
                    color: Color.fromRGBO(160, 160, 160, 1),
                  ),
                  SizedBox(height: 6),
                  TextWidget(
                    text:
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet neque blandit elit, eget aliquam ligula mi. Pellentesque id aliquam lorem nec interdum varius semper. Arcu urna ipsum etiam mauris, aliquam.",
                    color: Color.fromRGBO(24, 29, 39, 1),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
