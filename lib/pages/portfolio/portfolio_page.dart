import 'package:flutter/material.dart';

import 'package:gdi_app/widgets/portfolio_stack.dart';
import 'package:get/get.dart';
import 'portfolio_detail_page.dart';

import '../../widgets/default/text_widget.dart';

class PortfolioPage extends StatelessWidget {
  const PortfolioPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // iconTheme: const IconThemeData(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 75,
        title: const TextWidget(
          text: "Portfolio",
          size: 20,
          fw: FontWeight.w600,
          color: Colors.black,
        ),
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.black,
          ),
        ),
      ),

      body: ListView(
        physics: const BouncingScrollPhysics(),
        children: [
          PortfolioStack(
            projek: "Gemilang Total Optima",
            dev: "Web Development",
            widthgambar: double.infinity,
            heightgambar: 183,
            heightbutton: 57,
            pressed: () {
              Get.to(() => const PortfolioDetail());
            },
          ),
          PortfolioStack(
            projek: "Sayur Mama",
            dev: "Mobile Development",
            widthgambar: double.infinity,
            heightgambar: 183,
            heightbutton: 57,
            pressed: () {},
          ),
          PortfolioStack(
            projek: "BPOM Pontianak",
            dev: "Mobile & Web Development",
            widthgambar: double.infinity,
            heightgambar: 183,
            heightbutton: 57,
            pressed: () {},
          )
        ],
      ),
      // body: ListView.builder(
      //     itemCount: 2,
      //     itemBuilder: ((context, index) =>
      //         PortfolioStack(projek: "projek", dev: "dev"))),
    );
  }
}
