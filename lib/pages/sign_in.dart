// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:gdi_app/pages/homepage.dart';
import 'package:gdi_app/pages/sign_up.dart';

import 'package:gdi_app/widgets/image_logo.dart';
import 'package:gdi_app/widgets/submit_button.dart';

import 'package:local_auth/local_auth.dart';
import 'package:get/get.dart';

import '../widgets/default/text_form_widget.dart';
import '../widgets/default/text_widget.dart';

class SignIn extends StatefulWidget {
  const SignIn({Key? key}) : super(key: key);

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  bool isAuthenticated = false;
  LocalAuthentication localAuthentication = LocalAuthentication();

  bool isHidden = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.fromLTRB(30, 114, 0, 30),
              child: TextWidget(
                text: "Selamat Datang",
                size: 21,
                fw: FontWeight.w700,
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 30),
              child: TextWidget(
                text: "Yaaay! Masukkan data anda untuk Login",
                size: 13,
              ),
            ),

            //password
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 91, 20, 60),
              child: TextFormWidget(
                obscureText: isHidden,
                hint: "Masukkan Password",
                suffixIcon: InkWell(
                  onTap: passwordView,
                  child: Icon(
                    isHidden ? Icons.visibility_off : Icons.visibility,
                    color: Colors.blue,
                  ),
                ),
              ),
            ),

            //face rec
            Column(
              children: [
                IconButton(
                  iconSize: 45,
                  onPressed: pressed,
                  // icon: const Icon(
                  //   Icons.fingerprint,
                  //   color: Colors.blue,
                  // ),
                  icon: Image.asset(
                    "assets/images/facerec.png",
                    height: 35,
                    width: 35,
                  ),
                ),
                const SizedBox(height: 16),
                const TextWidget(text: "Face ID", size: 13)
              ],
            ),

            const SizedBox(height: 37),

            //forget password
            const TextWidget(
                text: "Lupa password?",
                size: 13,
                fw: FontWeight.w500,
                color: Colors.blue),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const TextWidget(
                    text: "Belum Punya Akun?", size: 13, color: Colors.blue),
                TextButton(
                    onPressed: () {
                      Get.to(() => const SignUp());
                    },
                    // style: ButtonStyle(
                    //   overlayColor: MaterialStateProperty.all(Colors.transparent),
                    // ),
                    child: const TextWidget(
                        text: "Buat Akun",
                        size: 13,
                        fw: FontWeight.w700,
                        color: Colors.blue)),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 55, 20, 27),
              child: SubmitButton(
                height: 54,
                width: double.infinity,
                text: "Login",
                pressed: () {
                  Get.to(() => HomePage());
                },
              ),
            ),
            // TextButton(
            //   onPressed: () {
            //     Get.to(() => const HomepageAdmin());
            //   },
            //   // style: ButtonStyle(
            //   //   overlayColor: MaterialStateProperty.all(Colors.transparent),
            //   // ),
            //   child: const TextWidget(
            //     text: "Login Admin",
            //     size: 13,
            //     color: Colors.blue,
            //   ),
            // ),
            const ImageLogo(),
          ],
        ),
      ),
    );
  }

  void passwordView() {
    setState(() {
      isHidden = !isHidden;
    });
  }

  void pressed() async {
    isAuthenticated = await localAuthentication.authenticate(
      localizedReason: "to login",
      options: const AuthenticationOptions(
        useErrorDialogs: true,
        stickyAuth: true,
        biometricOnly: true,
      ),
    );

    isAuthenticated ? Get.to(() => HomePage()) : Get.back();

    // setState(() {
    //   isAuthenticated ? Get.to(() => const Tiga()) : Text("data");
    // });
  }
}
