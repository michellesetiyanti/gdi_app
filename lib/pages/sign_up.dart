import 'package:flutter/material.dart';
import 'package:gdi_app/pages/sign_in.dart';
import 'package:gdi_app/widgets/default/text_widget.dart';

import 'package:get/get.dart';

import '../widgets/default/text_form_widget.dart';
import '../widgets/image_logo.dart';
import '../widgets/submit_button.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool isHidden = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 88, 20, 28),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const TextWidget(
                    text: "Sign Up",
                    size: 21,
                    fw: FontWeight.w700,
                  ),
                  Row(
                    children: [
                      const TextWidget(
                        text: "or",
                        size: 13,
                        fw: FontWeight.w500,
                      ),
                      TextButton(
                        onPressed: () {
                          Get.to(() => const SignIn());
                        },
                        child: const TextWidget(
                          text: "Sign In",
                          size: 13,
                          fw: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),

            //form sign up
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: TextFormWidget(
                          hint: "Nama Depan",
                        ),
                      ),
                      const SizedBox(
                        width: 7,
                      ),
                      Expanded(
                        child: TextFormWidget(
                          hint: "Nama Belakang",
                        ),
                      ),
                    ],
                  ),
                  TextFormWidget(
                    hint: "Email",
                    inputType: TextInputType.emailAddress,
                  ),
                  TextFormWidget(
                    hint: "Nomor Handphone",
                    inputType: TextInputType.phone,
                  ),
                  TextFormWidget(
                    obscureText: isHidden,
                    hint: "Masukkan Password",
                    suffixIcon: InkWell(
                      onTap: passwordView,
                      child: Icon(
                        isHidden ? Icons.visibility_off : Icons.visibility,
                        color: Colors.blue,
                      ),
                    ),
                  ),
                  TextFormWidget(
                    obscureText: isHidden,
                    hint: "Konfirmasi Password",
                    suffixIcon: InkWell(
                      onTap: passwordView,
                      child: Icon(
                        isHidden ? Icons.visibility_off : Icons.visibility,
                        color: Colors.blue,
                      ),
                    ),
                  ),
                  const SizedBox(height: 8),
                  const TextWidget(
                    text:
                        "Harus minimal 8 karakter termasuk huruf kecil, huruf besar dan atau sebuah simbol ( e.g ythhYT8!)",
                    color: Color.fromRGBO(85, 85, 85, 1),
                    size: 13,
                  )
                ],
              ),
            ),

            //sign up button
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 55, 20, 27),
              child: SubmitButton(
                height: 54,
                width: double.infinity,
                text: "Register",
                pressed: () {},
              ),
            ),

            const ImageLogo()
          ],
        ),
      ),
    );
  }

  void passwordView() {
    setState(() {
      isHidden = !isHidden;
    });
  }
}
