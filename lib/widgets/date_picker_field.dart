import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class DatePickerField extends StatefulWidget {
  const DatePickerField({Key? key}) : super(key: key);

  @override
  State<DatePickerField> createState() => _DatePickerFieldState();
}

class _DatePickerFieldState extends State<DatePickerField> {
  final TextEditingController dateinput = TextEditingController();
  @override
  void initState() {
    dateinput.text = ""; //set the initial value of text field
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(color: Color.fromRGBO(112, 144, 176, 0.2), blurRadius: 20),
        ],
      ),
      child: TextField(
        controller: dateinput,
        style: const TextStyle(fontSize: 12),
        decoration: InputDecoration(
          suffixIcon: const Icon(
            FontAwesomeIcons.calendarDay,
            color: Color.fromRGBO(193, 193, 193, 1),
          ),
          hintText: DateFormat('dd/MM/yyyy').format(DateTime.now()),
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10),
            borderSide: const BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          hintStyle: const TextStyle(
            color: Color.fromRGBO(142, 142, 142, 1),
            fontSize: 12,
          ),
        ),
        readOnly: true,
        onTap: () async {
          DateTime? pickedDate = await showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime(2000),
              lastDate: DateTime(2101));

          if (pickedDate != null) {
            String formattedDate = DateFormat('dd/MM/yyyy').format(pickedDate);

            setState(() {
              dateinput.text = formattedDate;
            });
          }
        },
      ),
    );
  }
}
