import 'package:flutter/material.dart';

class ContainerWidget extends StatelessWidget {
  const ContainerWidget({Key? key, this.radius, this.childd, this.paddding})
      : super(key: key);

  final BorderRadiusGeometry? radius;
  final Widget? childd;
  final EdgeInsetsGeometry? paddding;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: paddding,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: radius,
        color: Colors.white,
        boxShadow: const [
          BoxShadow(color: Color.fromRGBO(112, 144, 176, 0.2), blurRadius: 20),
        ],
      ),
      child: childd,
    );
  }
}
