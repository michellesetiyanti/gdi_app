// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class TextFormWidget extends StatelessWidget {
  TextFormWidget({
    Key? key,
    required this.hint,
    this.inputType,
    this.suffixIcon,
    this.obscureText = false,
  }) : super(key: key);

  String hint;
  TextInputType? inputType;
  Widget? suffixIcon;
  bool obscureText = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8),
      child: TextFormField(
        obscureText: obscureText,
        keyboardType: inputType,
        decoration: InputDecoration(
          suffixIcon: suffixIcon,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide:
                const BorderSide(color: Color.fromRGBO(224, 224, 224, 1)),
          ),
          hintText: hint,
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 20, vertical: 22),
          hintStyle: const TextStyle(
            color: Color.fromRGBO(85, 85, 85, 1),
            fontSize: 13,
          ),
        ),
      ),
    );
  }
}
