import 'package:flutter/material.dart';

class TextWidget extends StatelessWidget {
  const TextWidget(
      {Key? key,
      required this.text,
      this.size,
      this.fw,
      this.color,
      this.overflow})
      : super(key: key);

  final String text;
  final double? size;
  final FontWeight? fw;
  final Color? color;
  final TextOverflow? overflow;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      overflow: overflow,
      style: TextStyle(fontSize: size, fontWeight: fw, color: color),
    );
  }
}
