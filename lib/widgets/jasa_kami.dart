import 'package:flutter/material.dart';

import 'default/container_widget.dart';
import 'default/text_widget.dart';

class JasaKami extends StatelessWidget {
  const JasaKami({Key? key, required this.jasa}) : super(key: key);

  final String jasa;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 7.5),
      child: ContainerWidget(
        radius: BorderRadius.circular(10),
        childd: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextWidget(
                text: jasa,
                fw: FontWeight.w700,
                color: const Color.fromRGBO(85, 85, 85, 1),
              ),
              const SizedBox(height: 8),
              const TextWidget(
                text:
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet.",
                size: 12,
                color: Color.fromRGBO(160, 160, 160, 1),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
