// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:gdi_app/widgets/default/text_widget.dart';

class KaryawanCard extends StatefulWidget {
  const KaryawanCard({
    Key? key,
  }) : super(key: key);

  @override
  State<KaryawanCard> createState() => _KaryawanCardState();
}

class _KaryawanCardState extends State<KaryawanCard> {
  // bool isSelected = true;
  // final List<int> _list = List.generate(20, (i) => i);
  // List<bool> _selected = List.generate(200, (i) => false); // Pre filled list
  late int selectedIndex;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      // color:
      child: ListTile(
        // tileColor: _selected[index] ? Colors.amber : Colors.transparent,
        // selected: true,
        // onTap: () {
        //   setState(() {
        //     _selected[index] = !_selected[index];
        //   });
        // },
        leading: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Image.asset(
              'assets/images/anya.jpg',
              height: 50,
              width: 50,
              fit: BoxFit.cover,
            )),
        title: const TextWidget(
          text: "Nama",
          fw: FontWeight.w500,
          color: Color.fromRGBO(29, 29, 29, 1),
        ),
        subtitle: const TextWidget(
          text: "UI/UX Designer",
          size: 12,
          color: Color.fromRGBO(196, 196, 196, 1),
        ),
      ),
    );
  }
}
