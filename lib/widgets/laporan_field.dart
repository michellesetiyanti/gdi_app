import 'package:flutter/material.dart';

import 'default/text_widget.dart';

class LaporanField extends StatelessWidget {
  const LaporanField(
      {Key? key,
      required this.title,
      required this.hint,
      this.heightt,
      this.maxliness})
      : super(key: key);

  final String title, hint;
  final double? heightt;
  final int? maxliness;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextWidget(
          text: title,
          fw: FontWeight.w500,
          color: const Color.fromRGBO(24, 29, 39, 1),
        ),
        const SizedBox(height: 12),
        Container(
          height: heightt,
          decoration: const BoxDecoration(
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(112, 144, 176, 0.2), blurRadius: 20),
            ],
          ),
          child: TextField(
            maxLines: maxliness,
            decoration: InputDecoration(
              isCollapsed: true,
              filled: true,
              fillColor: Colors.white,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: const BorderSide(
                  width: 0,
                  style: BorderStyle.none,
                ),
              ),
              hintText: hint,
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 20, vertical: 22),
              hintStyle: const TextStyle(
                color: Color.fromRGBO(142, 142, 142, 1),
                fontSize: 12,
              ),
            ),
          ),
        )
      ],
    );
  }
}
