import 'package:flutter/material.dart';

import 'default/text_widget.dart';

class UploadFile extends StatelessWidget {
  const UploadFile({Key? key, this.pressed}) : super(key: key);

  final void Function()? pressed;

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      const TextWidget(
        text: "Bukti/Keterangan",
        fw: FontWeight.w500,
        color: Color.fromRGBO(24, 29, 39, 1),
      ),
      const SizedBox(height: 12),
      SizedBox(
        width: double.infinity,
        height: 134,
        child: TextButton(
          onPressed: pressed,
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(
              const Color.fromRGBO(0, 128, 255, 0.1),
            ),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Icon(
                Icons.cloud_upload,
                size: 45,
                color: Color.fromRGBO(0, 128, 255, 1),
              ),
              SizedBox(height: 8),
              TextWidget(text: "Masukkan data anda di sini", size: 12)
            ],
          ),
        ),
      ),
    ]);
  }
}
