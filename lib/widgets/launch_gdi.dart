import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'default/text_widget.dart';
import 'submit_button.dart';

class LaunchGDI extends StatelessWidget {
  const LaunchGDI({
    Key? key,
    required Uri url,
  })  : _url = url,
        super(key: key);

  final Uri _url;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.centerLeft,
      // fit: StackFit.expand,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 35, horizontal: 20),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(13),
              color: Colors.white,
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(112, 144, 176, 0.1),
                  blurRadius: 5,
                ),
              ],
            ),
            child: Image.asset(
              "assets/images/untitled.png",
            ),
          ),
        ),
        Container(
          // color: Colors.amber,
          padding: const EdgeInsets.symmetric(vertical: 35, horizontal: 46),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const TextWidget(
                text: "Ingin tahu lebih lanjut Tentang",
                size: 16,
                color: Color.fromRGBO(85, 85, 85, 1),
              ),
              const TextWidget(
                text: "PT Grha Digital Indonesia?",
                fw: FontWeight.w600,
                size: 16,
                color: Color.fromRGBO(85, 85, 85, 1),
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: SubmitButton(
                    text: "Klik Disini",
                    width: 104,
                    height: 34,
                    pressed: () async {
                      if (!await launchUrl(_url,
                          mode: LaunchMode.externalApplication)) {
                        throw 'Could not launch $_url';
                      }
                    }),
              )
            ],
          ),
        )
      ],
    );
  }
}
