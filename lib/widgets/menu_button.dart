import 'package:flutter/material.dart';
import 'package:gdi_app/widgets/default/text_widget.dart';

class MenuButton extends StatelessWidget {
  const MenuButton(
      {Key? key, required this.title, required this.icon, this.pressed})
      : super(key: key);

  final String title;
  final IconData icon;
  final void Function()? pressed;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            decoration: BoxDecoration(
              color: const Color.fromRGBO(255, 255, 255, 1),
              borderRadius: BorderRadius.circular(10),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(112, 144, 176, 0.1),
                  blurRadius: 20,
                ),
              ],
            ),
            child: IconButton(
              onPressed: pressed,
              icon: Icon(
                icon,
                size: 22,
                color: const Color.fromRGBO(0, 128, 255, 1),
              ),
            )),
        const SizedBox(height: 9),
        TextWidget(
          text: title,
          size: 11,
          color: const Color.fromRGBO(88, 88, 88, 1),
        )
      ],
    );
  }
}
