import 'package:flutter/material.dart';
import 'package:gdi_app/widgets/default/text_widget.dart';

class PortfolioStack extends StatelessWidget {
  const PortfolioStack(
      {Key? key,
      required this.projek,
      required this.dev,
      this.widthgambar,
      this.widthbutton,
      this.heightbutton,
      this.heightgambar,
      this.pressed})
      : super(key: key);

  final String projek;
  final String dev;
  final double? widthgambar;
  final double? widthbutton;
  final double? heightgambar;
  final double? heightbutton;
  final void Function()? pressed;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: Image.asset(
              "assets/images/pf.jpg",
              height: heightgambar,
              width: widthgambar,
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 35),
          child: SizedBox(
            height: heightbutton,
            width: widthbutton,
            child: ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(Colors.white),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextWidget(
                        text: projek,
                        size: 12,
                        overflow: TextOverflow.ellipsis,
                        fw: FontWeight.w700,
                        color: const Color.fromRGBO(88, 88, 88, 1),
                      ),
                      TextWidget(
                        text: dev,
                        size: 12,
                        overflow: TextOverflow.ellipsis,
                        fw: FontWeight.w700,
                        color: const Color.fromRGBO(160, 160, 160, 1),
                      ),
                    ],
                  ),
                  Container(
                    height: 34,
                    width: 34,
                    decoration: BoxDecoration(
                      color: const Color.fromRGBO(0, 128, 255, 1),
                      borderRadius: BorderRadius.circular(50),
                    ),
                    child: const Icon(Icons.chevron_right),
                  )
                ],
              ),
              onPressed: pressed,
            ),
          ),
        )
      ],
    );
  }
}
