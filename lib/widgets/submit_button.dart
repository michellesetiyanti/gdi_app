// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:gdi_app/widgets/default/text_widget.dart';

class SubmitButton extends StatelessWidget {
  SubmitButton(
      {Key? key, required this.text, this.pressed, this.width, this.height})
      : super(key: key);

  String text;
  void Function()? pressed;
  double? width;
  double? height;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: ElevatedButton(
        onPressed: pressed,
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(
            const Color.fromRGBO(0, 128, 255, 1),
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
            ),
          ),
        ),
        child: TextWidget(text: text, fw: FontWeight.w500, color: Colors.white),
      ),
    );
  }
}
