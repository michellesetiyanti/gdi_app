import 'package:flutter/material.dart';
import 'package:gdi_app/widgets/tracking_profile_photo.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import 'default/text_widget.dart';

class TrackingCard extends StatelessWidget {
  const TrackingCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      shadowColor: const Color.fromRGBO(112, 144, 176, 0.2),
      elevation: 20,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const TextWidget(
              text: "Juni 20, 2022",
              color: Color.fromRGBO(142, 142, 142, 1),
              fw: FontWeight.w500,
            ),
            const SizedBox(height: 16),
            const TextWidget(
              text: "Web Designing - BPOM Palu",
              size: 16,
              fw: FontWeight.w700,
            ),
            const SizedBox(height: 6),
            const TextWidget(
              text: "Prototyping",
              fw: FontWeight.w500,
              color: Color.fromRGBO(164, 164, 164, 1),
            ),
            const SizedBox(height: 17),
            const TextWidget(
              text: "Progress",
              fw: FontWeight.w700,
            ),

            //progress indicator
            LinearPercentIndicator(
              animation: true,
              animationDuration: 1000,
              lineHeight: 10,
              percent: 0.7,
              trailing: const TextWidget(
                text: "70%",
                fw: FontWeight.w500,
              ),
              backgroundColor: Colors.grey,
              progressColor: const Color.fromRGBO(0, 128, 255, 1),
            ),
            const SizedBox(height: 30),
            const Divider(
              height: 1,
              thickness: 1,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6),
                  child: Row(
                    children: const [
                      TrackingProfilePhoto(),
                      SizedBox(width: 2),
                      TrackingProfilePhoto(),
                      SizedBox(width: 2),
                      // Container(
                      //   height: 40,
                      //   width: 40,
                      //   decoration: BoxDecoration(
                      //     color: const Color.fromRGBO(0, 128, 255, 1),
                      //     borderRadius: BorderRadius.circular(50),
                      //   ),
                      //   child: IconButton(
                      //     onPressed: () {},
                      //     icon: const Icon(Icons.more_horiz_rounded,
                      //         color: Colors.white),
                      //   ),
                      // )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 6),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: const Color.fromRGBO(0, 128, 255, 0.2),
                    ),
                    child: const Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                      child: TextWidget(
                        text: "2 Hari lagi",
                        size: 12,
                        fw: FontWeight.w500,
                        color: Color.fromRGBO(0, 128, 255, 1),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
