import 'package:flutter/material.dart';

class TrackingProfilePhoto extends StatelessWidget {
  const TrackingProfilePhoto({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(50),
      child: Image.asset(
        "assets/images/anya.jpg",
        height: 40,
        width: 40,
        fit: BoxFit.cover,
      ),
    );
  }
}
