import 'package:flutter/material.dart';

class TrackingSearchBar extends StatelessWidget {
  const TrackingSearchBar({
    Key? key, this.hintText
  }) : super(key: key);

  final String? hintText;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      // height: 55,
      child: TextField(
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30),
            borderSide: const BorderSide(
              width: 0,
              style: BorderStyle.none,
            ),
          ),
          hintText: hintText,
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 20, vertical: 22),
          hintStyle: const TextStyle(
            color: Color.fromRGBO(142, 142, 142, 1),
            fontSize: 12,
          ),
          suffixIcon: Container(
            width: 55,
            height: 55,
            decoration: BoxDecoration(
              color: const Color.fromRGBO(0, 128, 255, 1),
              borderRadius: BorderRadius.circular(50),
            ),
            child: IconButton(
              onPressed: () {},
              icon: const Icon(Icons.search),
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
